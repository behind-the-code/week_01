import requests
import matplotlib.pyplot as plt
from PIL import Image
from io import BytesIO
import os


def rename_files(name_directory, new_name):
    control = 1
    files_list = os.listdir(name_directory)
    for old_file in files_list:
        os.rename(f'{name_directory}{old_file}',
                  f'{name_directory}{str(control).zfill(2)}_{new_name}')
        control = control+1
    return


def mkdir_dataset(list_directory):
    for name_directory in list_directory:
        os.makedirs(name_directory)
    return


def get_image(search_term, name_directory, number_get=100):
    subscription_key = "170edcda58af4fe4918e3a36ba3e13ac"
    search_url = "https://api.cognitive.microsoft.com/bing/v7.0/images/search"
    print(f'Search term: {search_term}')

    headers = {"Ocp-Apim-Subscription-Key": subscription_key}

    params = {"q": search_term}

    response = requests.get(search_url, headers=headers, params=params)
    response.raise_for_status()
    search_results = response.json()
    thumbnail_urls = [img["thumbnailUrl"]
                      for img in search_results["value"][:number_get]]

    print(len(thumbnail_urls))
    for idx, thumbnail_url in enumerate(thumbnail_urls):
        image_data = requests.get(thumbnail_url)
        image_data.raise_for_status()
        image = Image.open(BytesIO(image_data.content))
        image.save(
            f'{name_directory}{search_term}_{str(idx).zfill(3)}.jpg')

    return


list_directory = ['COCAMAR\dataset\lagarta',
                  'COCAMAR\dataset\percevejo_marrom',
                  'COCAMAR\dataset\percevejo_pequeno',
                  'COCAMAR\dataset\percevejo_verde']

# mkdir_dataset(list_directory)
# get_image('wild', "COCAMAR\dataset\\\\negativos\\", 10)

rename_files('COCAMAR\dataset\percevejo_marrom\\', 'percevejo_marrom.jpg')
